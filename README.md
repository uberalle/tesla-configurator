# Tesla Configurator - Angular Level 2 Certification

Welcome to the Angular Level 2 Certification mini-project - Car Configuration from angulartraining.com

This project aims to create a user-friendly web application for configuring Tesla vehicles. Whether you're customizing your dream car or exploring the latest features, this app provides an intuitive interface for a seamless experience.

## Features

- **Interactive Configurator**: Easily customize your Tesla vehicle with just a few clicks.
- **Real-time Updates**: Instantly see how your choices affect the final configuration.

## Getting Started

Follow these simple steps to get the project up and running on your local machine:

1. Clone this repository to your local machine.
2. Install dependencies by running `npm install`.
3. Start the development server with `ng serve`.
4. Open your browser and navigate to `http://localhost:4200/`.

## Usage

- Use the interactive interface to select your desired Tesla model and customize various features.
- Explore different color options, configs and packages and see your final configuration.
- See real-time updates as you make changes, allowing you to fine-tune your configuration.

## Feedback

Have any suggestions or found a bug? We'd love to hear from you! Please open an issue on Github or reach out to us directly.
