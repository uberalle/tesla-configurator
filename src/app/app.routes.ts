import { Routes } from '@angular/router';
import { StepsGuard } from './guards/steps.guard';

export const routes: Routes = [
  { path: 'step1',
    loadComponent: () => import('./components/step1/step1.component').then(c => c.Step1Component)
  },
  { path: 'step2',
    canActivate: [StepsGuard.step2Guard],
    loadComponent: () => import('./components/step2/step2.component').then(c => c.Step2Component)
  },
  { path: 'step3',
    canActivate: [StepsGuard.step3Guard],
    loadComponent: () => import('./components/step3/step3.component').then(c => c.Step3Component)
  },
  { path: '**', redirectTo: 'step1', pathMatch: 'full' }
];
