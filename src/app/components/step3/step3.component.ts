import { Component, Signal, computed, inject } from '@angular/core';
import { Model, ModelColor, ModelConfig } from '../../interfaces/model.interface';
import { AppService } from '../../app.service';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-step3',
  standalone: true,
  imports: [CurrencyPipe],
  templateUrl: './step3.component.html'
})
export class Step3Component {
  color: Signal<ModelColor>;
  model: Signal<Model>;
  modelConfig: Signal<ModelConfig>;
  totalCost: Signal<number>;
  towHitch: Signal<boolean>;
  yoke: Signal<boolean>;
  private readonly appService: AppService = inject(AppService);

  constructor() {
    this.color = computed(() => this.appService.color() as ModelColor);
    this.model = computed(() => this.appService.model() as Model);
    this.modelConfig = computed(() => this.appService.modelConfig() as ModelConfig);
    this.towHitch = computed(() => this.appService.towHitch());
    this.yoke = computed(() => this.appService.yoke());

    this.totalCost = computed(() => {
      const extrasPrice = (this.towHitch() ? 1000 : 0) + (this.yoke() ? 1000 : 0);
      return this.modelConfig().price + this.color().price + extrasPrice;
    });
  }
}
