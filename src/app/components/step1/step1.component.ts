import { Component, Signal, computed, inject } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { AsyncPipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, catchError, tap } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ColorCode, Model, ModelCode, ModelColor } from '../../interfaces/model.interface';
import { AppService } from '../../app.service';

@Component({
  selector: 'app-step1',
  standalone: true,
  imports: [ReactiveFormsModule, AsyncPipe],
  templateUrl: './step1.component.html'
})
export class Step1Component {
  colorCode: FormControl<ColorCode | null>;
  modelCode: FormControl<ModelCode | null>;
  modelColors: Signal<ModelColor[]>;
  models: Model[] = [];
  models$: Observable<Model[]>;
  modelsError: HttpErrorResponse | null = null;
  private readonly appService: AppService = inject(AppService);

  constructor() {
    this.modelColors = computed(() => this.appService.model()?.colors ?? []);
    this.modelCode = new FormControl<ModelCode | null>(this.appService.model()?.code ?? null);
    this.colorCode = new FormControl<ColorCode | null>(this.appService.color()?.code ?? null);

    this.models$ = this.appService.getAllModels().pipe(
      tap(models => this.models = models),
      catchError((error: HttpErrorResponse) => {
        this.modelsError = error;
        return [];
      })
    );

    this.modelCode.valueChanges.pipe(takeUntilDestroyed()).subscribe(modelCode => {
      this.appService.model = this.models.find((model) => model.code == modelCode) ?? null;
      const colors = this.modelColors();
      const color = colors.find((color) => color.code == this.appService.color()?.code) ??
          colors[0] ?? null;
      this.colorCode.setValue(color?.code ?? null);
      this.appService.color = color;
      this.appService.modelConfig = null;
      this.appService.towHitch = false;
      this.appService.yoke = false;
    });

    this.colorCode.valueChanges.pipe(takeUntilDestroyed()).subscribe(value => {
      this.appService.color = this.modelColors().find((color) => color.code == value) ?? null;
    });
  }
}
