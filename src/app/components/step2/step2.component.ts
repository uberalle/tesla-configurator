import { Component, Signal, computed, inject } from '@angular/core';
import { Observable, catchError, of, tap } from 'rxjs';
import { AsyncPipe, CurrencyPipe } from '@angular/common';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { RouterModule } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { AppService } from '../../app.service';
import { ModelCode, ModelConfig, ModelOptions } from '../../interfaces/model.interface';

@Component({
  selector: 'app-step2',
  standalone: true,
  imports: [ReactiveFormsModule, RouterModule, AsyncPipe, CurrencyPipe],
  templateUrl: './step2.component.html'
})
export class Step2Component {
  configId: FormControl<number | null>;
  modelConfig: Signal<ModelConfig | null>;
  modelOptions: ModelOptions | null = null;
  modelOptions$: Observable<ModelOptions | null>;
  modelOptionsError: HttpErrorResponse | null = null;
  towHitch: FormControl<boolean>;
  yoke: FormControl<boolean>;
  private readonly appService: AppService = inject(AppService);

  constructor() {
    this.modelConfig = computed(() => this.appService.modelConfig());
    this.configId = new FormControl(this.modelConfig()?.id ?? null);
    this.towHitch = new FormControl(this.appService.towHitch(), { nonNullable: true });
    this.yoke = new FormControl(this.appService.yoke(), { nonNullable: true });

    this.modelOptions$ = this.appService.getOptionsByModelCode(
      this.appService.model()?.code as ModelCode
    ).pipe(
      tap((options) => {
        this.modelOptions = options;
        this.appService.modelConfig =
            options.configs.find((config) => config.id == this.modelConfig()?.id) ??
            null;
      }),
      catchError((error: HttpErrorResponse) => {
        this.modelOptionsError = error;
        return of(null);
      })
    );
    
    this.configId.valueChanges.pipe(takeUntilDestroyed()).subscribe((currentConfigId) => {
      this.appService.modelConfig =
          this.modelOptions?.configs.find((config) => config.id == currentConfigId) ?? null;
    });

    this.towHitch.valueChanges.pipe(takeUntilDestroyed()).subscribe((hasTowHitch) => {
      this.appService.towHitch = hasTowHitch;
    });

    this.yoke.valueChanges.pipe(takeUntilDestroyed()).subscribe((value) => {
      this.appService.yoke = value;
    });
  }
}
