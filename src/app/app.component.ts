import { Component, Signal, computed } from '@angular/core';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { AppService } from './app.service';
import { Model, ModelColor, ModelConfig } from './interfaces/model.interface';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink, RouterLinkActive],
  templateUrl: './app.component.html',
})
export class AppComponent {
  color: Signal<ModelColor | null>;
  imageAlt: Signal<string | null>;
  imageUrl: Signal<string | null>;
  modelConfig: Signal<ModelConfig | null>;
  model: Signal<Model | null>;
  title = 'TeslaConfigurator';

  constructor(public appService: AppService) {
    this.model = computed(() => this.appService.model());
    this.color = computed(() => this.appService.color());
    this.modelConfig = computed(() => this.appService.modelConfig());

    this.imageAlt = computed(() => {
      const model = this.appService.model()?.description;
      const color = this.appService.color()?.description;

      if (!model || !color) {
        return null;
      }

      return  `Tesla ${model} - ${color}`;
    });

    this.imageUrl = computed(() => {
      const model = this.appService.model()?.code;
      const color = this.appService.color()?.code;

      if (!model || !color) {
        return null;
      }

      return `https://interstate21.com/tesla-app/images/${model}/${color}.jpg`;
    });
  }
}
