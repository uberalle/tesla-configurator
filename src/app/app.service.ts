import { HttpClient } from '@angular/common/http';
import { Injectable, Signal, inject, signal } from '@angular/core';
import { Observable, take } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { environment } from '../environments/environment';
import {
  Model, ModelCode, ModelOptions, ModelColor, ModelConfig
} from'./interfaces/model.interface';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private _color = signal<ModelColor| null>(null);
  set color(color: ModelColor | null) { this._color.set(color) };
  get color(): Signal<ModelColor | null> { return this._color };

  private _model = signal<Model| null>(null);
  set model(model: Model | null) { this._model.set(model) };
  get model(): Signal<Model | null> { return this._model };

  private _modelConfig = signal<ModelConfig | null>(null);
  set modelConfig(config: ModelConfig | null) { this._modelConfig.set(config) };
  get modelConfig(): Signal<ModelConfig | null> { return this._modelConfig };

  private _towHitch = signal<boolean>(false);
  set towHitch(value: boolean) { this._towHitch.set(value) };
  get towHitch(): Signal<boolean> { return this._towHitch };

  private _yoke = signal<boolean>(false);
  set yoke(value: boolean) { this._yoke.set(value) };
  get yoke(): Signal<boolean> { return this._yoke };

  private readonly http: HttpClient = inject(HttpClient);

  public getAllModels(): Observable<Model[]> {
    return this.http.get<Model[]>(`${environment.baseUrl}models`).pipe(
      take(1), takeUntilDestroyed()
    );
  }

  public getOptionsByModelCode(modelCode: ModelCode): Observable<ModelOptions>{
    return this.http.get<ModelOptions>(`${environment.baseUrl}options/${modelCode}`,).pipe(
      take(1), takeUntilDestroyed()
    );
  }
}
