export type ModelCode = 'S' | 'X' | 'C' | '3' | 'Y';
export type ColorCode = 'white' | 'black' | 'blue' | 'grey' | 'red';

export interface ModelColor {
  code: ColorCode;
  description: string;
  price: number;
}

export interface Model {
  code: ModelCode;
  description: string;
  colors: ModelColor[];
}

export interface ModelConfig {
  id: number;
  description: string;
  range: string;
  speed: number;
  price: number;
}

export interface ModelOptions {
  configs: ModelConfig[];
  towHitch: boolean;
  yoke: boolean;
}
