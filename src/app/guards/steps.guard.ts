import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AppService } from '../app.service';

export namespace StepsGuard {

  export const step2Guard: CanActivateFn = () => {
    const appService = inject(AppService);
    const router = inject(Router);
    const isValid = appService.model() !== null && appService.color() !== null;

    if (!isValid) {
      router.navigate(['/step1']);
    }

    return isValid;
  };

  export const step3Guard: CanActivateFn = () => {
    const appService = inject(AppService);
    const router = inject(Router);
    const isValid =  appService.model() !== null && appService.color() !== null &&
        appService.modelConfig() !== null;

    if (!isValid) {
      router.navigate(['/step2']);
    }

    return isValid;
  };
}
