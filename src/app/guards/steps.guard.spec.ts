import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { StepsGuard } from './steps.guard';

describe('stepsGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => StepsGuard.step2Guard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
